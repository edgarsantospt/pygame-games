import pygame
from pygame import mixer
import random
import math

# initialize pygame
pygame.init()

print("Initializing")

# create the screen
# note: top-left screen coordinates
screen = pygame.display.set_mode((800, 600))

# background
background = pygame.image.load('background.jpg')

# background sound
mixer.music.load('backgroundMusic.wav')
mixer.music.play(-1)

# title and icon
pygame.display.set_caption("Space Invaders")
icon = pygame.image.load('001-ufo.png')
pygame.display.set_icon(icon)

# score_value
score_value = 0
high_score_value = 0
font = pygame.font.Font('freesansbold.ttf', 32)

textX = 10
textY = 10

highScoreTextX = 800 - 235
highScoreTextY = textY

# player
# ed comment: values in pixels, no "render pipeline" at all
playerImg = pygame.image.load('002-spaceship.png')
playerX = 370
playerY = 480
playerSpeed = 0.3
# ed comment: the initial position should be done in percentages of the total width and height

# bullet
# ready => no bullet on screen
# fired => bullet on screen
bulletImg = pygame.image.load('004-bullet.png')
bulletX = 0
bulletY = 0
bulletX_speed = 0
bulletY_speed = 0.75
bullet_state = "ready"


def is_collision(enemy_x, enemy_y, bullet_x, bullet_y):
    distance = calculate_distance(enemy_x, enemy_y, bullet_x, bullet_y)
    if distance < 32:
        return True
    else:
        return False


def calculate_distance(x0, y0, x1, y1):
    return math.sqrt(math.pow(x0 - x1, 2) + math.pow(y0 - y1, 2))


# enemies
class Enemy:
    def __init__(self, enemy_img, enemy_x, enemy_y, enemy_x_speed, enemy_y_speed):
        self.enemyImg = enemy_img
        self.enemyX = enemy_x
        self.enemyY = enemy_y
        self.enemyX_change = enemy_x_speed
        self.enemyY_change = enemy_y_speed


num_of_enemies = 10
enemies = []
# balanced value: 0.01
speed_increase = 0.01


# avoid spawning ships on top of each other
def create_spawn_position():
    sprite_size = 64
    center_offset = sprite_size / 2

    x_min = 0
    x_max = 800 - sprite_size
    y_min = 50
    y_max = 100

    enemy_x = random.randint(x_min, x_max)
    enemy_y = random.randint(y_min, y_max)
    is_overlapping = True
    while is_overlapping:
        is_overlapping = False
        for e in enemies:
            distance = calculate_distance(e.enemyX + center_offset, e.enemyY - center_offset,
                                          enemy_x + center_offset, enemy_y - center_offset)
            if distance < 64:
                is_overlapping = True
                enemy_x = random.randint(x_min, x_max)
                enemy_y = random.randint(y_min, y_max)
    return enemy_x, enemy_y


def reset():
    # reset enemies + player + bullets
    global enemies
    global bullet_state
    global playerX
    global playerY
    global score_value
    global pressingLeft
    global pressingRight

    # reset enemies
    enemies.clear()
    for i in range(num_of_enemies):
        spawn_position = create_spawn_position()

        enemy_instance = Enemy(
            pygame.image.load('003-ufo-1.png'),
            spawn_position[0],
            spawn_position[1],
            0.15,
            50)
        enemies.append(enemy_instance)

    # reset bullet
    bullet_state = "ready"

    # reset player
    playerX = 370
    playerY = 480

    # reset score
    score_value = 0

    # reset input movement
    pressingLeft = False
    pressingRight = False


def show_score(x, y):
    score = font.render("Score: " + str(score_value), True, (255, 255, 255))
    screen.blit(score, (x, y))


def show_high_score(x, y):
    score = font.render("High Score: " + str(high_score_value), True, (255, 255, 255))
    screen.blit(score, (x, y))


def show_game_over(x, y):
    game_over_txt = font.render("GAME OVER", True, (255, 255, 255))
    screen.blit(game_over_txt, (x, y))
    instruction_txt = font.render("Press SPACE to restart the game...", True, (255, 255, 255))
    screen.blit(instruction_txt, (x, y + 40))


def render_player(x, y):
    screen.blit(playerImg, (x, y))


def render_enemy(x, y, img):
    screen.blit(img, (x, y))


def render_bullet(x, y):
    screen.blit(bulletImg, (x, y))


def fire_bullet(x, y):
    # global is needed to disambiguate between a local variable shadowing naming and the actual global var
    global bullet_state, bulletX, bulletY

    bullet_state = "fired"
    bulletX = x + 16
    bulletY = y - 16
    # (16, 16) offset to make it spawn in the middle and above of the ship (32/2)


def restrict_horizontal_movement(x):
    if x <= 0:
        x = 0
    elif x >= 736:
        x = 736
    return x


# ed fix: quick fix to avoid the "no change when swapping directions" issue
# basically detecting presses in the input update and dealing with it after, tutorial had a very sketchy way of doing it
pressingLeft = False
pressingRight = False

reset()

# game over
gameOver = False

# Game loop
running = True


def is_game_over(en):
    if en.enemyY >= playerY - 64:
        return True
    return False


while running:

    # clear -----------------------------------
    # color values are from 0 to 255, not normalized
    screen.fill((0, 0, 0))
    # background image
    screen.blit(background, (0, 0))

    # game over -------------------------------
    if gameOver:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    gameOver = False
                    reset()

        # game over render
        show_game_over(200, 200)

        todo = True
    else:
        # check input -----------------------------
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    playerX_change = -playerSpeed
                    pressingLeft = True
                if event.key == pygame.K_RIGHT:
                    playerX_change = playerSpeed
                    pressingRight = True
                if event.key == pygame.K_SPACE:
                    if bullet_state == 'ready':
                        fire_bullet(playerX, playerY)
                        bullet_sound = mixer.Sound('shot.wav')
                        bullet_sound.set_volume(0.2)
                        bullet_sound.play()
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT:
                    pressingLeft = False
                if event.key == pygame.K_RIGHT:
                    pressingRight = False

        # game elements update --------------------

        # bullet update
        if bulletY <= 0:
            bullet_state = 'ready'
        if bullet_state == 'fired':
            bulletY -= bulletY_speed

        # player update
        if pressingLeft:
            playerX -= playerSpeed
        if pressingRight:
            playerX += playerSpeed
        playerX = restrict_horizontal_movement(playerX)

        # enemy update
        for enemy in enemies:
            enemy.enemyX += enemy.enemyX_change
            if enemy.enemyX <= 0:
                enemy.enemyX_change = -enemy.enemyX_change
                enemy.enemyY += enemy.enemyY_change
                enemy.enemyX_change += speed_increase
            if enemy.enemyX >= 800 - 64:
                enemy.enemyX_change = -enemy.enemyX_change
                enemy.enemyY += enemy.enemyY_change
                enemy.enemyX_change -= speed_increase
            # for performance reasons, detecting game over in the enemy iteration
            if is_game_over(enemy):
                gameOver = True
                # high score update
                if high_score_value < score_value:
                    high_score_value = score_value

        # collisions / "physics" update
        if bullet_state == 'fired':
            for enemy in enemies:
                # IMPORTANT: fixing the coordinates to be the center of the sprite and not the top left coordinates
                # enemy sprite is 64x64 and bullet sprite is 32x32
                collision = is_collision(enemy.enemyX+32, enemy.enemyY-32, bulletX+16, bulletY-16)
                if collision:
                    bullet_state = 'ready'

                    # explosion sound
                    explosion_sound = mixer.Sound('explosion.wav')
                    explosion_sound.set_volume(0.2)
                    explosion_sound.play()

                    # score change
                    score_value += 1

                    # print(score_value)
                    spawnPos = create_spawn_position()
                    print("New spawn position:(" + str(spawnPos[0]) + "," + str(spawnPos[1]) + ")")
                    enemy.enemyX = spawnPos[0]
                    enemy.enemyY = spawnPos[1]
                    break

        # in-game render --------------------------------
        render_player(playerX, playerY)
        for enemy in enemies:
            render_enemy(enemy.enemyX, enemy.enemyY, enemy.enemyImg)
        if bullet_state == 'fired':
            render_bullet(bulletX, bulletY)

    # common render elements
    show_score(textX, textY)
    show_high_score(highScoreTextX, highScoreTextY)
    pygame.display.update()
