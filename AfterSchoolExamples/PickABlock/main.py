import random
import pygame

##########################
### CONFIGURATION DATA ###
##########################

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 900
RESOLUTION_SCALING_FACTOR = 0.90

FRAMES_PER_SECOND = 30

POINTS_SECTION_HEIGHT = 100
TIME_SECTION_HEIGHT = 50

GRID_WIDTH = 10
GRID_HEIGHT = 10

BACKGROUND_COLOR = pygame.Color(0, 0, 0)
GRID_COLOR = pygame.Color(255, 255, 255)
PLAYER_COLOR = pygame.Color(255, 0, 0)
COIN_COLOR = pygame.Color(255, 255, 0)
OBSTACLE_COLOR = pygame.Color(100, 100, 100)
POINTS_LABEL_COLOR = pygame.Color(255, 255, 0)
TIME_LABEL_COLOR = pygame.Color(255, 255, 0)

COIN_POINTS_VALUE = 5
NUMBER_OF_OBSTACLES = 10

GAME_DURATION_SECONDS = 30

SMOOTH_MOVEMENT_TIME_UNSCALED_SPEED_0_1 = 0.5


############################################
### DATA DERIVED FROM CONFIGURATION DATA ###
############################################

SCREEN_WIDTH = int(SCREEN_WIDTH * RESOLUTION_SCALING_FACTOR)
SCREEN_HEIGHT = int(SCREEN_HEIGHT * RESOLUTION_SCALING_FACTOR)
GRID_CELL_WIDTH = SCREEN_WIDTH / GRID_WIDTH
GRID_CELL_HEIGHT = (SCREEN_HEIGHT - POINTS_SECTION_HEIGHT) / GRID_HEIGHT

COIN_RADIUS = max(
    GRID_CELL_WIDTH/2,
    GRID_CELL_HEIGHT/2
)


########################
### HELPER FUNCTIONS ###
########################

def get_random_position_in_grid():
    """
        Returns a dictionary representing a random position in the map
    """
    x = random.randint(0, GRID_WIDTH-1)
    y = random.randint(0, GRID_HEIGHT-1)
    return pygame.Vector2(x, y)


def get_random_positions_in_grid(n, exception_list):
    """
        Returns a list of dictionaries representing random positions in the map
    """
    list_of_positions = []
    generated_positions = 0
    while generated_positions < n:
        random_pos = get_random_position_in_grid()
        if (not list_of_positions.__contains__(random_pos)) and (not exception_list.__contains__(random_pos)):
            list_of_positions.append(random_pos)
            generated_positions += 1
    return list_of_positions


def clamp(value, minimum, maximum):
    """
        Returns a value "limited" by a minimum and a maximum.
        Ensures the following rule:
            minimum <= value <= maximum
        Examples:
            clamp(12, 10, 20) => 12
            clamp(5, 10, 20) => 10
            clamp(123, 10, 20) => 20
    """
    if minimum >= maximum:
        error_message = "'minimum' must be less than 'maximum': minimum='{}' maximum='{}'".format(minimum, maximum)
        raise ValueError(error_message)

    if value < minimum:
        return minimum
    if value > maximum:
        return maximum
    return value

#####################
### GAME ELEMENTS ###
#####################

# GRID

def draw_grid(surface):
    # Draws the game grid.
    # Note the `SCREEN_HEIGHT-POINTS_SECTION_HEIGHT` bit. We want the grid to
    # occupy the whole screen except the bit at the bottom for the points.

    for i in range(GRID_WIDTH+1):
        x_to_draw_vertical_line = GRID_CELL_WIDTH * i
        pygame.draw.line(
            surface=surface,
            color=GRID_COLOR,
            start_pos=(x_to_draw_vertical_line, 0),
            end_pos=(x_to_draw_vertical_line, SCREEN_HEIGHT-POINTS_SECTION_HEIGHT),
            width=5
        )

    for i in range(GRID_HEIGHT+1):
        y_to_draw_horizontal_line = GRID_CELL_HEIGHT * i
        pygame.draw.line(
            surface=surface,
            color=GRID_COLOR,
            start_pos=(0, y_to_draw_horizontal_line),
            end_pos=(SCREEN_WIDTH, y_to_draw_horizontal_line),
            width=5
        )


# Player

def update_player(player_position, inputs, obstacles):
    proposed_position = pygame.Vector2(player_position)
    if inputs['right'] == True:
        proposed_position.x += 1
    if inputs['left'] == True:
        proposed_position.x -= 1

    # Remember that Y increases as we go to the bottom of the screen
    if inputs['down'] == True:
        proposed_position.y += 1
    if inputs['up'] == True:
        proposed_position.y -= 1

    # We also don't want the player to go "outside" of the map
    proposed_position.x = clamp(proposed_position.x, 0, GRID_WIDTH - 1)
    proposed_position.y = clamp(proposed_position.y, 0, GRID_HEIGHT - 1)

    # We only "accept" the proposed position if it didn't land on an obstacle
    if not obstacles.__contains__(proposed_position):
        player_position = proposed_position

    return player_position


def draw_player(surface, player_intended_position, player_current_draw_position):
    # This is called a "coordinate transformation"
    # Given a position on the grid, What is the corresponding position in the screen
    # Games with more complex screen layouts require more complex transformations!
    #
    # This one in particular, gives us the top-left corner of the grid cell

    vector_intended = pygame.Vector2(player_intended_position.x * GRID_CELL_WIDTH,
                                     player_intended_position.y * GRID_CELL_HEIGHT)

    vector_current = pygame.Vector2(vector_intended)
    if not (player_current_draw_position is None):
        vector_current = pygame.Vector2(player_current_draw_position)

    vector_draw = vector_current.lerp(vector_intended, SMOOTH_MOVEMENT_TIME_UNSCALED_SPEED_0_1)

    player_rect = pygame.Rect(
        vector_draw.x,
        vector_draw.y,
        GRID_CELL_WIDTH,
        GRID_CELL_HEIGHT
    )

    pygame.draw.rect(
        surface,
        PLAYER_COLOR,
        player_rect,
    )

    return pygame.Vector2(vector_draw)


# Coin

def get_new_coin_position(positions_where_coin_cant_get_spawn):
    new_coin_position = get_random_position_in_grid()
    # We have to make sure that the new coin does not spawn on the same position as the player.
    while positions_where_coin_cant_get_spawn.__contains__(new_coin_position):
        new_coin_position = get_random_position_in_grid()
    return new_coin_position


def draw_coin(surface, coin_position):
    # Once again, a coordinate transformation.
    # This gives us the top-left corner of the grid cell
    coin_x_in_screen = coin_position.x * GRID_CELL_WIDTH
    coin_y_in_screen = coin_position.y * GRID_CELL_HEIGHT

    # But to dra a circle we need the center of it.
    # So lets get the coordinates of the center of the cell
    coin_in_screen_centered_in_cell = pygame.Vector2(
        coin_x_in_screen + (GRID_CELL_WIDTH/2),
        coin_y_in_screen + (GRID_CELL_HEIGHT/2))

    pygame.draw.circle(
        surface,
        COIN_COLOR,
        coin_in_screen_centered_in_cell,
        COIN_RADIUS
    )


# Obstacles

def draw_obstacles(surface, obstacle_positions):
    for obstacle in obstacle_positions:
        # drawing in a similar way to the player, please see the comments there for more details
        obstacle_x_in_screen = obstacle.x * GRID_CELL_WIDTH
        obstacle_y_in_screen = obstacle.y * GRID_CELL_HEIGHT

        obstacle_rect = pygame.Rect(
            obstacle_x_in_screen,
            obstacle_y_in_screen,
            GRID_CELL_WIDTH,
            GRID_CELL_HEIGHT
        )

        pygame.draw.rect(
            surface,
            OBSTACLE_COLOR,
            obstacle_rect
        )


# Points Label

def draw_points_label(surface, font, points):
    message = 'Points: {}'.format(points)
    text_surface = font.render(message, True, POINTS_LABEL_COLOR)
    surface.blit(text_surface, (0, SCREEN_HEIGHT-POINTS_SECTION_HEIGHT))


# Time label

def draw_time_label(surface, font, time):
    message = 'Time left: {}'.format(int(time))
    text_surface = font.render(message, True, TIME_LABEL_COLOR)
    surface.blit(text_surface, (0, SCREEN_HEIGHT-TIME_SECTION_HEIGHT))

###################
### GAME SCENES ###
###################

# Game scenes have their own state and loop.
# The game has only one scene "running"
# And a scene, when it ends, returns the next scene to run
# (and maybe some more things)

def title_scene(screen, clock):
    # This scene is pretty simple, so I'll just have all the code it need in this function
    title_font = pygame.font.SysFont('Comic Sans MS', 70)
    title_text = title_font.render("Get The Gold!", True, (255, 255, 255)) # TODO: title color should be constant

    instruction_font = pygame.font.SysFont('Comic Sans MS', 30)
    instruction_text = instruction_font.render("Press [ENTER] to start.", True, (255, 255, 255))

    # TODO: Using dictionaries is too error prone (and returning strings too, we should at least have constants defined)
    while True:
        inputs = get_processed_input()
        if inputs['quit']:
            return 'QUIT'

        if inputs['enter']:
            # if the player presses ENTER, we go to the game scene
            return 'GAME'

        # DRAW
        screen.fill(BACKGROUND_COLOR)
        screen.blit(
            title_text,
            ( ((SCREEN_WIDTH/2) - (title_text.get_width()/2)) , 300 )
        )

        screen.blit(
            instruction_text,
            ( ((SCREEN_WIDTH/2) - (instruction_text.get_width()/2)) , 450 )
        )

        pygame.display.flip()

        clock.tick(30)


def game_over_scene(screen, clock, points):
    # This scene is pretty simple, so I'll just have all the code it need in this function
    message = "You got {} points!".format(points)
    title_font = pygame.font.SysFont('Comic Sans MS', 70)
    title_text = title_font.render(message, True, (255, 255, 255))

    instruction_font = pygame.font.SysFont('Comic Sans MS', 30)
    instruction_text = instruction_font.render("Press [ENTER] to start again.", True, (255, 255, 255))

    while True:
        inputs = get_processed_input()
        if inputs['quit']:
            return 'QUIT'

        if inputs['enter']:
            # if the player presses ENTER, we go to the game scene
            return 'GAME'

        # DRAW
        screen.fill(BACKGROUND_COLOR)
        screen.blit(
            title_text,
            ( ((SCREEN_WIDTH/2) - (title_text.get_width()/2)) , 300 )
        )

        screen.blit(
            instruction_text,
            ( ((SCREEN_WIDTH/2) - (instruction_text.get_width()/2)) , 450 )
        )

        pygame.display.flip()

        clock.tick(30)


def game_scene(screen, clock):
    # The main scene, that handles the "game" part
    font = pygame.font.SysFont('Comic Sans MS', 30)

    # Setup the starting player position, the first coin, reset the points and the time counter
    player_position = get_random_position_in_grid()
    player_draw_position = None
    coin_position = get_new_coin_position([player_position])

    # Setup obstacles anywhere except in the player position and the coin position
    obstacle_positions = get_random_positions_in_grid(NUMBER_OF_OBSTACLES, [player_position, coin_position])

    points = 0
    time_in_game = 0

    while True:

        # Increment the time counter and check if enough time has passed to finish the game.
        time_in_game += clock.get_time()  # this value is in milliseconds
        time_left_in_game = GAME_DURATION_SECONDS - (time_in_game / 1000)  # time_in_game converted to seconds
        if time_left_in_game <= 0:
            return 'GAME_OVER', points

        inputs = get_processed_input()
        if inputs['quit']:
            return 'QUIT'

        # UPDATE GAME ELEMENTS

        player_position = update_player(player_position, inputs, obstacle_positions)
        # coin doesn't need an update function
        # obstacles don't need an update function

        # UPDATE GAME STATE

        # pick up coin
        if player_position == coin_position:  # The player is overlapping the coin
            forbidden_coin_positions = [player_position]
            forbidden_coin_positions += obstacle_positions  # append obstacle positions to the list
            coin_position = get_new_coin_position(forbidden_coin_positions)
            points += COIN_POINTS_VALUE


        # DRAW

        screen.fill(BACKGROUND_COLOR)
        player_draw_position = draw_player(screen, player_position, player_draw_position)
        draw_coin(screen, coin_position)
        draw_obstacles(screen, obstacle_positions)
        draw_grid(screen)
        draw_points_label(screen, font, points)
        draw_time_label(screen, font, time_left_in_game)

        pygame.display.flip()

        clock.tick(30)


###################################
### CORE LOOP & GAME FLOW LOGIC ###
###################################

def get_processed_input():
    inputs = {
        # keys
        'left': False,
        'right': False,
        'up': False,
        'down': False,
        'enter': False,

        # quit application
        'quit': False
    }

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            inputs['quit'] = True
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                inputs['left'] = True
            elif event.key == pygame.K_RIGHT:
                inputs['right'] = True
            elif event.key == pygame.K_UP:
                inputs['up'] = True
            elif event.key == pygame.K_DOWN:
                inputs['down'] = True
            elif event.key == pygame.K_RETURN:
                inputs['enter'] = True

    return inputs

def main():
    # SETTING UP THE GAME
    pygame.init()
    pygame.font.init()

    screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
    clock = pygame.time.Clock()
    current_scene = 'TITLE'
    last_points = -1

    # core loop
    while True:
        if current_scene == 'TITLE':
            current_scene = title_scene(screen, clock)
        elif current_scene == 'GAME':
            current_scene, last_points = game_scene(screen, clock)
        elif current_scene == 'GAME_OVER':
            current_scene = game_over_scene(screen, clock, last_points)
        else:
            break


if __name__ == '__main__':
    main()
